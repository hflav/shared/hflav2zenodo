from setuptools import setup, find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='hflav2zenodo',
    license='Apache License Version 2.0',
    version='0.0.b4',
    packages=find_packages(),
    url='https://gitlab.cern.ch/hflav/hflav2zenodo',
    author='Ulrik Egede',
    install_requires=['requests', 'responses'],
    entry_points = {
        "console_scripts": [
            "g2z-send = hflav2zenodo.main:g2z_command",
            "g2z-get-meta = hflav2zenodo.main:g2z_meta_command",
        ]
    },
    description='Sends gitlab snapshots to zenodo Automatically.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires='>=3.10',
    keywords='zenodo gitlab',
    classifiers=["Development Status :: 4 - Beta",
                 "Intended Audience :: Developers",
                 "Intended Audience :: Science/Research",
                 "License :: OSI Approved :: Apache Software License",
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3",
                 "Programming Language :: Python :: 3.10",
                 "Programming Language :: Python :: 3.11",
                 ]
)
